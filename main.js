'use strict'

const { app, BrowserWindow } = require('electron');
const { autoUpdater } = require('electron-updater');
const path = require('path')

var url = require('url');
let win;


function createWindow() {
  // Create the browser window.
  win = new BrowserWindow({
    backgroundColor: '#ffffff',
    useContentSize: true,
    resizable: true,
    autoHideMenuBar: true,
    height: 600,
    width: 800,
    show:false,
    title: 'Metha Viewer',
    icon: `${__dirname}/build/coding.ico`
  });





  win.loadURL(url.format({
    pathname: path.join(__dirname, 'build', 'index.html'),
    protocol: 'file:',
    slashes: true
  }));

  // uncomment below to open the DevTools.
  win.webContents.openDevTools();

  // Event when the window is closed.
  win.on('closed', function () {
    win = null
  });

  win.on('ready-to-show', () => { 
    win.show();
  });

  autoUpdater.checkForUpdates();
}



// Create window on electron intialization
app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', function () {

  // On macOS specific close process
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', function () {
  // macOS specific close process
  if (win === null) {
    createWindow()
  }
});

  
autoUpdater.on('checking-for-update', () => {

});


autoUpdater.on('update-available', (ev, info) => {
  console.log('update available');
  // let msg = {
  //   type: 'update-available',
  //   from: 'main',
  //   data: 'update-available'
  // }
  // connection.send(JSON.stringify(msg));
});


autoUpdater.on('update-not-available', (ev, info) => {
  console.log('update not available');
  // let msg = {
  //   type: 'update-not-available',
  //   from: 'main',
  //   data: 'update-not-available'
  // }
  // connection.send(JSON.stringify(msg));
});


autoUpdater.on('error', (ev, err) => {
  console.log(`update error1 - ${error}`);
  // let msg = {
  //   type: 'update-error',
  //   from: 'main',
  //   data: JSON.stringify(err)
  // }
  // connection.send(JSON.stringify(msg));
});


autoUpdater.on('download-progress', (ev, progressObj) => {

  console.log(`progress = ${progressObj}`);
  // let msg = {
  //   type: 'download-progress',
  //   from: 'main',
  //   data: JSON.stringify(ev)
  // }
  // connection.send(JSON.stringify(msg));
});

autoUpdater.on('update-downloaded', (ev, info) => {
    console.log(`downloaded`); 
    autoUpdater.quitAndInstall();
});
