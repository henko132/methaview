import { MatCheckboxModule, MatExpansionModule, MatTableModule,MatNativeDateModule,MatGridListModule, MatDatepickerModule, MatSlideToggleModule, MatCardModule, MatInputModule, MatButtonModule, MatButtonToggleModule, MatTabsModule, MatMenuModule, MatToolbarModule, MatSidenavModule, MatIconModule, MatTooltipModule, MatOptionModule, MatDialogModule, MatSelectModule, MatProgressBarModule, MatRadioModule, MatAutocompleteModule, MatChipsModule, MatListModule, MatSnackBarModule, MatProgressSpinnerModule,MatStepperModule, MatSortModule } from '@angular/material';

export const ExtModules = [
    MatCheckboxModule,
    MatExpansionModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatCardModule,
    MatInputModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatSlideToggleModule,
    MatTabsModule,
    MatMenuModule,
    MatToolbarModule,
    MatSidenavModule,
    MatIconModule,
    MatTooltipModule,
    MatOptionModule,
    MatDialogModule,
    MatSelectModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatAutocompleteModule,
    MatChipsModule,
    MatListModule,
    MatSnackBarModule,
    MatStepperModule,
    MatGridListModule,
    MatTableModule,
    MatSortModule
    //package used for force-selected of auto-complete - material does not support

]


export const SharedComponents = [
  
];

export const SharedEntryComponents = [
   
]
   

export const SharedDirectives = [

];

export const SharedPipes = [

]
