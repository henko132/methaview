import { NgModule} from "@angular/core";
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule} from '@angular/forms';

import { SharedComponents, SharedDirectives, SharedPipes, ExtModules,SharedEntryComponents } from './shared.declarations';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        
        ...ExtModules
    ],
    entryComponents: [...SharedEntryComponents],
    declarations: [...SharedComponents, ...SharedPipes, ...SharedDirectives], 
    exports: [CommonModule, ...SharedComponents,
        FormsModule,
        ReactiveFormsModule,        
       
        ...SharedComponents, ...SharedDirectives, ...SharedPipes, ...ExtModules
    ]
})

export class SharedModule { }

