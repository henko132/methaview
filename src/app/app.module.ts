import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../modules/shared/shared.module';
import { FlexLayoutModule} from '@angular/flex-layout';
import { ROUTES } from './app.router';
import { Components, EntryComponents } from './app.declarations';
import { AppComponent } from './app.component';

import { ExifService,SharedService,FileService } from './providers';
import { ElectronService } from 'ngx-electron';


@NgModule({
  declarations: [
    AppComponent,
    ...Components,...EntryComponents
  ],
  imports: [
    BrowserAnimationsModule, 
    RouterModule,
    RouterModule.forRoot(ROUTES),
    FlexLayoutModule,    
    SharedModule
  ],
  exports: [
    FlexLayoutModule
  ],
  providers: [
    ElectronService,
    ExifService,
    SharedService,
    FileService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
