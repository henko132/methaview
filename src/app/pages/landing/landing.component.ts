import { Component } from '@angular/core';
import { ExifService, SharedService,FileService } from '../../providers';
import { ExifItem } from '../../models/exif';
import { ElectronService } from 'ngx-electron';



@Component({
    selector: 'landing.component',
    templateUrl: 'landing.component.html',
    styleUrls: ['./landing.component.scss']
})
export class LandingComponent {
    public clawHasFile: boolean = false;
    public files: any[] = [];
    public selectedView: number = 1;
    public searchText: string = '';

    constructor(
        public exifService: ExifService,
        public sharedService: SharedService,
        private fileService:FileService
    ) { }

    ngOnInit() {
        
     }

    draggingOver(event) {
        event.preventDefault();
        event.stopPropagation();
        this.clawHasFile = true;
    }

    draggingAway(event) {
        this.clawHasFile = false;
        event.preventDefault();
        event.stopPropagation();
    }

    dropped(event) {
        event.preventDefault();
        event.stopPropagation();
        this.clawHasFile = false;
        for (let file of event.dataTransfer.files) {
            let existingFile = this.files.filter(x => x.filename == file.name)[0];
            if (existingFile) {
                this.sharedService.selectedTabIndex = this.files.indexOf(existingFile);
            }
            else {
                this.exifService.readrawMeta(file.path).then((fileMeta: ExifItem[]) => {
                    this.files.push(
                        {
                            filename: file.name,
                            meta: fileMeta
                        }
                    );
                });
            }
        }
    }

    removeFile(file: any) {
        let index = this.files.indexOf(file);

        if (index != -1) {
            this.files.splice(index, 1);
        }
    }



    clearFiles() {
        this.files = [];
    }

    triggerSearch(event) {
        this.sharedService.metaSearchUpdate.next(event);
    }

    public selectFile(e) {
        this.clawHasFile = false;
        for (let file of e.target.files) {
            let existingFile = this.files.filter(x => x.filename == file.name)[0];
            if (existingFile) {
                this.sharedService.selectedTabIndex = this.files.indexOf(existingFile);
            }
            else {
                this.exifService.readrawMeta(file.path).then((fileMeta: ExifItem[]) => {
                    this.files.push(
                        {
                            filename: file.name,
                            meta: fileMeta
                        }
                    );
                });
            }
        }
    }

    public getFiles(){
        this.fileService.getFiles();
    }

    
}