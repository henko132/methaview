import { Component, OnInit } from '@angular/core';
import { ExifItem } from '../../models/exif';
import { SharedService } from '../../providers/shared.service';
@Component({
    selector: 'flat-list-view',
    templateUrl: 'flat-list-view.component.html',
    styleUrls: [
        './flat-list-view.component.scss'
    ],
    inputs: ['metadata']
})
export class FlatListViewComponent implements OnInit {

    public metadata: ExifItem[] = []
    public filteredFileMeta: ExifItem[] = [];
    constructor(
        private sharedService: SharedService
    ) { }

    ngOnInit() {
        this.filteredFileMeta = this.metadata;
        this.sharedService.metaSearchUpdate.subscribe(val => {
            if (val) {
                if (val.length > 0) {
                    this.filteredFileMeta = val ? this.metadata.filter((imb) =>
                     new RegExp(val, 'gi').test(imb.group)
                    || new RegExp(val, 'gi').test(imb.name)
                    || new RegExp(val, 'gi').test(imb.value)
                ) : this.metadata;
                }
                else {
                    this.filteredFileMeta = this.metadata;
                }    
            }
        });
    }
}