import { Component, OnInit } from '@angular/core';
import { SharedService } from '../../providers';
import { ExifItem } from '../../models/exif';
@Component({
    selector: 'group-list-view',
    templateUrl: 'group-list-view.component.html',
    styleUrls: ['./group-list-view.component.scss'],
    inputs: ['metadata']
})
export class GroupListViewComponent implements OnInit {

    public metadata: ExifItem[] = []
    public filteredGroupMeta: GroupedMeta[] = [];
    public filteredFileMeta: ExifItem[] = [];
    constructor(
        private sharedService: SharedService
    ) { }

    ngOnInit() {
        this.filteredFileMeta = this.metadata;
        this.groupMeta();
        this.sharedService.metaSearchUpdate.subscribe(val => {
            if (val) {
                if (val.length > 0) {
                    this.filteredFileMeta = val ? this.metadata.filter((imb) =>
                        new RegExp(val, 'gi').test(imb.group)
                        || new RegExp(val, 'gi').test(imb.name)
                        || new RegExp(val, 'gi').test(imb.value)
                    ) : this.metadata;
                }
                else {
                    this.filteredFileMeta = this.metadata;
                }

                this.groupMeta();
            }
        });
    }

    public groupMeta() {
        this.filteredGroupMeta = [];
      
        for (let metaitem of this.filteredFileMeta) {
            let count = this.filteredGroupMeta.filter(x => x.groupName == metaitem.group).length;
            if (count == 0) {
                this.filteredGroupMeta.push(
                    {
                        groupName: metaitem.group,
                        groupMeta: this.metadata.filter(x => x.group == metaitem.group)
                    });
            }
        }
    }
}

export class GroupedMeta {
    groupName: string;
    groupMeta: ExifItem[];
}