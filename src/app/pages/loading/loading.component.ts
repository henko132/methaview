import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'loading',
    templateUrl: 'loading.component.html',
    styleUrls:['./loading.component.scss']
})
export class LoadingComponent {
    constructor(
        private router: Router,
    ) { }

    ngOnInit() { 

        // setTimeout(() => {
            this.router.navigate(['app']);
        // }, 3000);
    }
}