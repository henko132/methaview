import { Component, OnInit, ViewChild } from '@angular/core';
import { ExifItem } from '../../models/exif';
import { SharedService } from '../../providers';
import { MatTableDataSource, MatSort } from '@angular/material';

@Component({
    selector: 'details-list-view',
    templateUrl: 'details-list-view.component.html',
    styleUrls: ['./details-list-view.component.scss'],
    inputs: ['metadata']
})

export class DetailsListViewComponent implements OnInit {
    public metadata: ExifItem[] = []
   // public filteredFileMeta: ExifItem[] = [];
    @ViewChild(MatSort) sort: MatSort;

    public displayedColumns: string[] = ['group', 'name', 'value'];
    public dataSource;
   

    constructor(
        private sharedService: SharedService
    ) { }

    ngOnInit() {
        //this.filteredFileMeta = this.metadata;
        this.dataSource = new MatTableDataSource(this.metadata);
        this.sharedService.metaSearchUpdate.subscribe(val => {
            if (val) {
                if (val.length > 0) {
                    this.dataSource.filter = val;
                //     this.filteredFileMeta = val ? this.metadata.filter((imb) =>
                //      new RegExp(val, 'gi').test(imb.group)
                //     || new RegExp(val, 'gi').test(imb.name)
                //     || new RegExp(val, 'gi').test(imb.value)
                // ) : this.metadata;
                }
                else {
                    //this.filteredFileMeta = this.metadata;
                }    
            }

            //console.table(this.filteredFileMeta);
        });

        this.dataSource.sort = this.sort;
    }
}