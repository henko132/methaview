import { LoadingComponent, LandingComponent, FlatListViewComponent,GroupListViewComponent,DetailsListViewComponent } from './pages';



export const Components = [
  LoadingComponent,
  LandingComponent,
  FlatListViewComponent,
  GroupListViewComponent,
  DetailsListViewComponent
];

export const EntryComponents = [
];

