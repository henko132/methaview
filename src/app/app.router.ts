import { Routes } from '@angular/router';
import { LandingComponent,LoadingComponent } from './pages';
export const ROUTES: Routes = [
    {
        path: '',
        component: LoadingComponent
    },
    {
        path: 'app',
        component: LandingComponent
    }
];


