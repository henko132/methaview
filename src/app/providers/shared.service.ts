import { Injectable } from '@angular/core';
import { BehaviorSubject } from '../../../node_modules/rxjs';

@Injectable()
export class SharedService {
    metaSearchUpdate: BehaviorSubject<string> = new BehaviorSubject<string>(undefined);
    selectedTabIndex: number = 0;
}