import { Injectable } from '@angular/core';
import { ElectronService } from 'ngx-electron';
import { environment } from '../../environments/environment';
import { ExifTags,ExifItem } from '../models/exif';

@Injectable()
export class ExifService {

    private childProcess = this.electronService.remote.require('child_process');
    public busy: boolean = false;

    constructor(private electronService: ElectronService) { }

    public readrawMeta(path: string) {
        
        return new Promise((resolve, reject) => {
            this.busy = true;

            let returnMeta: ExifItem[] = [];
            let response = "";
            let errorMessage = "";
            let exif = this.childProcess.spawn(`${environment.exifFilePath }`, ["-fast","-G","-t" ,"-m","-q","-u",path]);

            exif.on('error', (err) => {
                return reject(err);
            });

            exif.stdout.on("data",  (data)=>{
                response += data;
            });

            exif.stderr.on("data",  (data)=> {
                errorMessage += data.toString();
            });
        
            exif.on("close", () => {
                
                 var lines = response.split('\n');
                 for (var line = 0; line < lines.length; line++) {
                    let text: string = lines[line];
                    if (text.length > 0) {
                        let rawExifItems = text.split('\t');
                        
                        let exifItem: ExifItem = {
                            group : rawExifItems[0],
                            name: rawExifItems[1],
                            value:rawExifItems[2]
                        }
                        returnMeta.push(exifItem);
                    }
                 }
                 this.busy = false;
                return resolve(returnMeta);
            });
        });
    }
   
}