import { Injectable } from '@angular/core';
import { ElectronService } from 'ngx-electron';
@Injectable()
export class FileService {

    private fs = this.electronService.remote.require('fs');

    constructor(private electronService: ElectronService) {
    
    }


    private workspaces: string[] = [
        "C:/Users/HenkoGermishuizen/Documents",
        "C:/Users/HenkoGermishuizen/Desktop/images/Pretoria Rain"
    ]

    public getFiles() {
        let items: any[] = [];
        this.workspaces.forEach((workspace: string) => {
            this.fs.readdir(workspace, ["utf8"], (err, data) => {
                if (err) 
                    console.log(`An error has occurred - ${err}`);
                
                else {
                    console.log(data);

                }

             });
        });
    }
}