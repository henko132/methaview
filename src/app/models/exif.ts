export interface ExifTags{
    tagName: string;
    tagValue: string;
}

export interface ExifItem{
    group: string;
    name: string;
    value: string;
}
